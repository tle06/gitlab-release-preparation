#!/usr/local/bin/python
import gitlab
import argparse
import re
import os

# https://semver.org/

def extract_digit_from_tag(tag:str):
    temp = re.findall(r'\d+', tag)
    return list(map(int, temp))

def increase_version(num:list,limit:int):
    if num < limit:
        return num + 1
    else:
        return 0

def get_gitlab_release_tag(server_url:str,token:str, project_id:int):

    try:
        gl = gitlab.Gitlab(server_url, private_token=token)
        gl.auth()
    except Exception as e:
        print(f"[ERROR] Gitlab connection failure: {e}")
        print(f"[ERROR] Check if the server URL and token values are correct")
        exit(1)
    
    gl_project = gl.projects.get(id=project_id)
    gl_release = gl_project.releases.list()
    
    return gl_release[0].tag_name

def main():

    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument('-s', '--semantic',
                        default='patch',
                        choices=['major', 'minor', 'patch'],
                        dest='version_type',
                        help='version type https://semver.org/',
                        type=str
                        )
    parser.add_argument('-l', '--limit',
                        default=99,
                        dest='limit',
                        help='the limit where the upper version will get +1, default 99.',
                        type=int
                        )
    parser.add_argument('-r', '--disable-reset-lower',
                        dest='disable_reset_lower',
                        action='store_false',
                        help='Disable the reset of minor and patch if increase version is major or minor, default true',
                        )
    parser.add_argument('-o', '--output',
                        default='tag.txt',
                        dest='output',
                        help='output path for tag',
                        type=str
                        )
    parser.add_argument('-m', '--manual-tag',
                        dest='manual_tag',
                        help='Set tag manually instead of reading from gitlab',
                        type=str
                        )
    parser.add_argument('-u', '--url',
                        default="https://gitlab.com/",
                        dest='server_url',
                        help='The gitlab server url',
                        type=str
                        )
    parser.add_argument('-p', '--project_id',
                        default=os.getenv('CI_PROJECT_ID'),
                        dest='project_id',
                        help='The gitlab project ID to retreived the issues, default env var CI_PROJECT_ID',
                        type=str
                        )
    parser.add_argument('-t', '--token',
                        dest='token',
                        help='the gitlab token to authenticate',
                        required=True,
                        type=str
                        )                                         
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Verbose Output'
                        )
    args = parser.parse_args()
    
    # Args to var

    version_type:str = args.version_type
    gitlab_server_url: str = args.server_url
    gitlab_project_id: str = args.project_id
    gitlab_token: str = args.token
    output:str = args.output
    version_limit: int = args.limit
    disable_reset_lower:bool = args.disable_reset_lower
    manual_tag:str = args.manual_tag if args.manual_tag else None
    
    verboseprint = print if args.verbose else lambda *a, **k: None
    verboseprint("Start script")
    verboseprint(f"Arg version-type: {version_type}")
    verboseprint(f"Arg server_url: {gitlab_server_url}")
    verboseprint(f"Arg project_id: {gitlab_project_id}")
    verboseprint(f"Arg output: {output}")
    verboseprint(f"Arg limit: {version_limit}")
    verboseprint(f"Arg disable-reset-lower: {disable_reset_lower}")
    verboseprint(f"Arg manual-tag: {manual_tag}")


    if not manual_tag:
        verboseprint(f"No manual tag detected, reading gitlab release")
        verboseprint(f"Get gitlab project id: {gitlab_project_id}")
        last_release_tag_name: str = get_gitlab_release_tag(server_url=gitlab_server_url,token=gitlab_token,project_id=gitlab_project_id)
    else:
        last_release_tag_name = manual_tag
    verboseprint(f"Last release tag name: {last_release_tag_name}")

    
    tag_digits_list:list = extract_digit_from_tag(tag=last_release_tag_name)
    version_major:int = tag_digits_list[0]
    version_minor:int = tag_digits_list[1]
    version_patch:int = tag_digits_list[2]

    verboseprint(f"Current version major : {version_major}")
    verboseprint(f"Current version minor : {version_minor}")
    verboseprint(f"Current version patch : {version_patch}")
    verboseprint(f"Switch limit : {version_limit}")
    verboseprint(f"Reset lower version : {disable_reset_lower}")

    if version_type == 'major':
        version_major = version_major + 1

        if disable_reset_lower:
            verboseprint(f"Version minor set to 0")
            version_minor = 0
            verboseprint(f"Version minor set to 0")
            version_patch = 0
    
    if version_type == "minor":
        version_minor = increase_version(version_minor,limit=version_limit)
        if disable_reset_lower:
            verboseprint(f"Version patch set to 0")
            version_patch = 0
            
        if version_minor == 0:
            version_major = version_major + 1

    if version_type == "patch":
        version_patch = increase_version(version_patch,limit=version_limit)

        if version_patch == 0:
            version_minor = increase_version(version_minor,limit=version_limit)
            if version_minor == 0:
                version_major = version_major + 1



    new_tag: str = f"v{version_major}.{version_minor}.{version_patch}"
    verboseprint(f"New tag generated: {new_tag}")

    verboseprint(f"Wrinting description to output file: {output}")
    print(new_tag)
    file = open(output, 'w')
    file.writelines(new_tag+'\n')
    file.close()

    verboseprint("Done")

if __name__ == "__main__":
    main()