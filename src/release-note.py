#!/usr/local/bin/python

import gitlab
import os
import shutil
import argparse
from os.path import exists

# source: https://python-gitlab.readthedocs.io/en/stable/api-usage.html

def main():
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument('-s', '--source',
                        dest='source',
                        help='source template of the release description',
                        required=True,
                        type=str
                        )
    parser.add_argument('-o', '--output',
                        default='output.md',
                        dest='output',
                        help='output path for the template',
                        type=str
                        )
    parser.add_argument('-p', '--project_id',
                        default=os.getenv('CI_PROJECT_ID'),
                        dest='project_id',
                        help='The gitlab project ID to retreived the issues, default env var CI_PROJECT_ID',
                        type=str
                        )
    parser.add_argument('-t', '--token',
                        dest='token',
                        help='The gitlab token to authenticate',
                        required=True,
                        type=str
                        )
    parser.add_argument('-u', '--url',
                        default="https://gitlab.com/",
                        dest='server_url',
                        help='The gitlab server url',
                        type=str
                        )
    parser.add_argument('-r', '--open-mode',
                        default='a',
                        dest='open_mode',
                        help='python open mode value, default a',
                        type=str
                        )
    parser.add_argument('-S', '--state',
                        default="closed",
                        dest='issue_state',
                        help='The state value filter to retreive the project issue',
                        type=str
                        )                                                                       
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Verbose Output'
                        )
    args = parser.parse_args()
    open_mode:str = args.open_mode
    gitlab_server_url: str = args.server_url
    gitlab_project_id: str = args.project_id
    gitlab_token: str = args.token
    output:str = args.output
    source:str = args.source
    description: list = []
    
    verboseprint = print if args.verbose else lambda *a, **k: None
    verboseprint("Start script")
    verboseprint(f"Arg server_url: {gitlab_server_url}")
    verboseprint(f"Arg project_id: {gitlab_project_id}")
    verboseprint(f"Arg output: {output}")
    verboseprint(f"Arg source: {source}")
    verboseprint(f"Arg open-mode: {open_mode}")

    try: 
        gl = gitlab.Gitlab(gitlab_server_url, private_token=gitlab_token)
        gl.auth()
    except Exception as e:
        print(f"[ERROR] Gitlab connection failure: {e}")
        print(f"[ERROR] Check if the server URL and token values are correct")
        exit(1)


    verboseprint("Get gitlab project")
    gl_project = gl.projects.get(id=gitlab_project_id)
    gl_issues = gl_project.issues.list(state=args.issue_state)
    gl_release = gl_project.releases.list()

    last_release_date: str = gl_release[0].released_at
    verboseprint(f"Last release date: {last_release_date}")

    for issue in gl_issues:
        verboseprint(f"Issue title: {issue.title}")
        verboseprint(f"Issue closed at: {issue.closed_at}")

        if issue.closed_at > last_release_date:
            verboseprint("issue.closed_at is > to last_release_date")
            verboseprint("Adding title to description list")
            description.append(f"* {issue.title}")

    verboseprint(f"Description expected: {description}")

    if not exists(source):
        print(f"Template source doesn't exist at: {source}")
        exit(1)
    
    
    verboseprint(f"Reading template")
    with open(source) as f:
        template_content = f.readlines()
    
    output_file = open(output, open_mode)

    verboseprint(f"Wrinting template to output file:{output}")
    for line in template_content:
        output_file.writelines(line+'\n')

    verboseprint(f"Wrinting description to output file:{output}")
    for line in description:
        output_file.writelines(line+'\n')
    output_file.close()

    verboseprint("Done")


if __name__ == "__main__":
    main()
