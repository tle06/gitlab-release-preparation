[![pipeline status](https://gitlab.com/tle06/gitlab-release-preparation/badges/v.1.0.3/pipeline.svg)](https://gitlab.com/tle06/gitlab-release-preparation/-/commits/v.1.0.3)
[![coverage report](https://gitlab.com/tle06/gitlab-release-preparation/badges/v.1.0.3/coverage.svg)](https://gitlab.com/tle06/gitlab-release-preparation/-/commits/v.1.0.3)

# Gitlab release preparation

Set of tool to prepare a gitlab release to be used with the gitlab [release-cli](https://gitlab.com/gitlab-org/release-cli) tool

1. Generate next available tag
2. Generate release description with the issues closed from the last release


## 1. Generate next available tag

The script `release-tag` will connect to the gitlab API and retrieved the latest release tag of the project to increase the [version semantic](https://semver.org/) by one. You can increase the patch, minor and major version number.

By default the script will read the project ID from the env var `$CI_PROJECT_ID`.
Your can also specify a different one with `-p` option.

### 1.1 The limit

The script will limit the increase to 99 by default and increase the next semantic with one.

Example:

Your current version is `v1.0.99` then if you do `release-tag -v -s patch` then the new version will be `v1.1.0`.


### 1.2 Usage

```shell
release-tag -h
```

```shell
# Increase the patch value by one
# If current version: v1.0.35

release-tag -v -t $TOKEN -s patch
v1.0.36
```

## 2. Generate release description

The script `release-note` will retrieve all the issues closed from the last release date. If your last release is at `2021-10-03T13:35:58.219Z` and there are 2 closed issues since this date, the script will get the title of those issues and add the corresponding markdown into the template file of your choice.

By default the script will read the project ID from the env var `$CI_PROJECT_ID`. Your can also specify a different one with `-p` option.

### 2.1 usage

```shell
release-tag -h
```

```shell
release-tag -v -t $TOKEN -s ./template/release-description.md
```

The output of the previous cmd will be:

``` markdown
# Issues closed

- issue title 1
- issue title 2
```

## 3. How to use in your CI/CD

1. Generate a personal token with `api` scope and add it in your CI/CD environment
2. Add the following job in your `.gitlab-ci.yaml` file.

```yaml

stages:
  - prepare-release
  - release

# preparation of the tag and description
release:prep:
  image: registry.gitlab.com/tle06/gitlab-release-preparation:latest
  stage: prepare-release
  script:
    - release-tag -v -t $GITLAB_GIT_USER_TOKEN -s patch
    - release-note -v -s ./template/release-description.md -o template.md -t $GITLAB_GIT_USER_TOKEN
  artifacts:
    paths:
      - template.md
      - tag.txt

# create the release with the previous tag and description generated
release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs: ["release:prep"]
  dependencies:
    - release:prep
  before_script:
    - export TAG=$(cat tag.txt)
  script:
    - release-cli create --name "Release $TAG" --tag-name $TAG --description ./template.md
```
## 4. Docker usage

Pull the image from registry
```shell
docker pull registry.gitlab.com/tle06/gitlab-release-preparation:latest
```

release-tag
```shell
docker run -i --rm registry.gitlab.com/tle06/gitlab-release-preparation:latest \ release-tag -v -t "my-gitlab-token" -s patch
```
release-note
```shell
docker run -i --rm registry.gitlab.com/tle06/gitlab-release-preparation:latest \ release-note -v -t "my-gitlab-token" -s ./template/release-description.md
```