FROM python:3.9.7-slim-buster
LABEL maintainer="tle@tlnk.fr"

WORKDIR /tmp
COPY src/ .

RUN chmod +x *.py
RUN find -type f -name '*.py' | while read f; do mv "$f" "${f%.py}"; done
RUN mv release* /usr/local/bin/
RUN pip install -r requirements.txt

LABEL org.label-schema.name="gitlab-release-preparation"
LABEL org.label-schema.description="Gitlab release preparation tools"
LABEL org.label-schema.url="https://gitlab.com/tle06/gitlab-release-preparation"
LABEL org.label-schema.vendor="TLNK"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vcs-url="https://gitlab.com/tle06/gitlab-release-preparation"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.docker.cmd="docker run registry.gitlab.com/tle06/gitlab-release-preparation"